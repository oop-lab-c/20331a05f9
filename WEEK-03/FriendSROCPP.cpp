#include<iostream>
using namespace std;
class Box{
    private:
        float length;
        float width;
        float height;
    public:
    inline void displayWelcomeMessage()
    {
        cout<<"Box proprties"<<endl;
    }
    // as a member function defiend inside class
    void boxArea(float length, float width)
    {
        cout<<"Area of a box : "<<length*width<<endl;
    }
   void boxVolume(float length, float width, float height); 
};
// as a member function defined oustide class
void Box::boxVolume(float length, float width, float height)
{
    cout<<"Volume of a box :"<<length*width*height<<endl;
}
int main()
{
    float length,width,height;
    Box obj1;
    obj1.displayWelcomeMessage();
    cout<<"enter the  length of a box: ";
    cin>>length;
    cout<<"enter the width of a box: ";
    cin>>width; 
    cout<<"enter a height of a box: ";
    cin>>height;
    obj1.boxArea(length,width);
    obj1.boxVolume(length,width,height);
    
    return 0;
}
