#include<iostream>
using namespace std;
class base1
{
    public:
    void parent1()
    {
        cout<<"Hello Parent1"<<endl;
    }
};
class base2
{
    public:
    void parent2() 
#include<iostream>
using namespace std;
class base{
    private:
    int x=1;
    public:
    int y=2;
    protected:
    int z=3;
    int getX() {
        return x;
    }
    int getY() {
        return y;
    }
    int getZ() {
        return z;
    }
};
class publicderv: public base
{
    public:
    int getZ()
    {
        return z;
    }
};
class protectedderv:protected base
{
    public:
    int getY()
    {
        return y;
    }
    int getZ()
    {
        return z;
    }
};
class privatederv : private base
{
     public:
    int getY()
    {
        return y;
    }
    int getZ()
    {
        return z;
    }
};
int main()
{
    publicderv obj1;
    cout<<"....PUBLIC INHERITANCE...."<<endl;
    //cout << "Private = " << obj1.getX() << endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj1.getZ() << endl;
    cout << "Public = " << obj1.y << endl;
    protectedderv obj2;
    cout<<"....PROTECTED INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj2.getZ() << endl;
    cout << "Public = " << obj2.getY() << endl;
    privatederv obj3;
    cout<<"....PRIVATE INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj3.getZ() << endl;
    cout << "Public = " << obj3.getY() << endl;
    return 0;
}


   