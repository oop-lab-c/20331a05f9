// Program to demonstrate the types of inheritence
import java.util.*;
class Base
{
    void fun1()
    {
        System.out.println("Base class method is called");
    }
}
class Derived extends Base
{
    void fun()
    {
        System.out.println("Derived class method is called");
    }
}
class Derived1 extends Base
{
    void fun2()
    {
        System.out.println("Derived1 class method is called");
    }
}
class Derived2 extends Derived1
{
   void fun3()
   {
    System.out.println("Derived2 class method is called");
   }
}
class Derived3 extends Derived
{
    void fun4()
    {
        System.out.println("Derived3 class method is called");
    }
}
 /*class Derived4 extends Derived2,Derived3
   {

   }*/
class InheriTypesJava
{
public static void main(String[] args)
{
    Derived1 obj1 = new Derived1();
    System.out.println("SINGLE INHERITENCE : "); // single inheritence
    obj1.fun1();
    obj1.fun2();
    // Multiple inheritence can't be acheived using extends keyword
    Derived2 obj2 = new Derived2();
    System.out.println("MULTILEVEL INHERITENCE : "); // multilevel inheritence 
    obj2.fun1();
    obj2.fun2();
    obj2.fun3();
    Derived obj = new Derived();
    System.out.println("HIERARCHIAL INHERITENCE : "); // hierarchial inheritence
    obj.fun();
    obj.fun1();
    obj1.fun1();
    obj1.fun2();
    Derived3 obj3 = new Derived3();
    System.out.println("HYBRID INHERITENCE : "); // hybrid inheritence
    obj.fun();
    obj.fun1();
    obj1.fun1();
    obj1.fun2();
    obj3.fun();
    obj3.fun1();
}
}
