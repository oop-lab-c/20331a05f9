#include<iostream>
using namespace std;
class parent
{
    public:
    void display()
    {
        cout<<"Iam a parent class"<<endl;
    }
};
class child: public parent
{
    public:
    void display()
    {
        cout<<"Iam a child class";
    }
};
int main()
{
    child obj1;
    obj1.display();
}