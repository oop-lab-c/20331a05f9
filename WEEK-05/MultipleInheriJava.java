import java.util.*;
interface GrandParent//Base class
{
    public void display();
}
//Hierarchical Inheritance
interface Parent1 extends GrandParent
{
    default void display()
    {
        System.out.println("I am interface of parent2");
    }
}
interface Parent2 extends GrandParent
{
    default void display()
    {
        System.out.println("I am  Interface of parent2");
    }
}
//Multiple Inheritance
class Child implements Parent1,Parent2 
{
    public void display()
    {
        System.out.println("I am GrandParent class");
        Parent1.super.display();
        Parent2.super.display();
    }
}
class MultipleInheriJava
{
    public  static void main(String[] args)
    {
        Child obj=new Child();//Creating an object for child class
        obj.display();
    }
}
