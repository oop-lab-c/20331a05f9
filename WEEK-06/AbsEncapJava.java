import java.util.*;
class AccessSpecifierDemo
{
    private int priVar;
    protected int proVar;
    int pubVar;
    public void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public void getVar()
    {
        System.out.println(priVar);
        System.out.println(proVar);
        System.out.println(pubVar);
    }  
}
class AbsEncapJava
{
    public static void main(String[] args)
    {
        AccessSpecifierDemo obj=new AccessSpecifierDemo();
        int priValue=10,proValue=20,pubValue=30;
        obj.setVar(priValue,proValue,pubValue);
        obj.getVar();
    }
}
