import java.util.*;
class Table{    
  //synchronized static method to print squares of numbers
  synchronized static void printTable(int n)
  {  
   for(int i=1;i<=10;i++)
   {  
     System.out.print(n*i + " ");  
     try
     {  
       Thread.sleep(100);
     }catch(Exception e)
     {
         System.out.println("Thread Interrupted");
     }  
   } 
   System.out.println();
 }  
} 
  
//thread class 
class Thread1 extends Thread
{  
    public void run()
    {  
        Table.printTable(2);  
    }  
}
   
//thread class
class Thread2 extends Thread
{  
    public void run()
    { 
        Table.printTable(5);  
    }  
}  
   
public class ThreadSyncJava{  
public static void main(String[] args){
        //create instances of Thread1 and Thread2
        Thread1 t1=new Thread1();  
        Thread2 t2=new Thread2(); 
        //start each thread instance
        t1.start();  
        t2.start();  
    }  
}  