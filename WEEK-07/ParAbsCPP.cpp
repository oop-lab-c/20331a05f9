#include<iostream>
using namespace std;
class animal{
    public:
    virtual void bark()
    {
        cout<<"animal can bark";
    };
    virtual void walk()
    {
        
    };
};
class dog: public animal
{
    public:
    void bark()
    {
        cout<<"Bow Bow"<<endl;
    }
    void walk()
    {
        cout<<"Dog can walk"<<endl;
    }
};
int main()
{
    dog obj;
    obj.bark();
    obj.walk();
}