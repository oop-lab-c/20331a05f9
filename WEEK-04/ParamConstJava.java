
import java.lang.*;
import java.util.Scanner;
class Student
{
    String name;
    double sempercent;
    String cname;
    int ccode;
    Student()
    {
        cname="mvgr";
        ccode=33;
        System.out.println("college name : "+cname);
        System.out.println("college code : "+ccode);
    }
    Student(String sname,double percent)
    {
        name=sname;
        sempercent=percent;
    }
    protected void finalize()
    {
        System.out.println("Finalize method is invoked");
    }
    public static void main(String[] args)
    {
        Student obj1=new Student();
        Student obj2=new Student("sandeep",95.0);
        System.out.println("student name:"+ obj2.name);
        System.out.println("sempercent:"+obj2.sempercent);
    }
};
