#include<iostream>
//we have include <iomanip> to allow the C++ manipulators
#include<iomanip>
using namespace std;
int main()
{
    //endl manipulator is used to Terminate a line and flushes the buffer
    cout<<"Hello manipulators"<<endl;
    //without endl
    cout<<"line1 ";
    cout<<"line2 "<<endl;
    //setw manipulator sets the width of the filed assigned for the output.
    cout<< setw(10) <<2222<<"\n";
    //setfill character is used in output insertion operations to fill spaces when results have to be padded to the field width.
    cout<<"setw() & setfill type-1"<<endl;
    cout<< setfill('0');
	cout<< setw(10) <<2222<<"\n";
	cout<<"setw() & setfill type-2"<<endl;
	cout<< setfill('*')<< setw(10) <<2222<<"\n";
    return 0;
}