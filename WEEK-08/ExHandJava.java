public class builtinException {  
  
    public static void main(String[] args) {  
          
           try{    
                int a[]=new int[5];    
                a[5]=30/0; 
                String s=null;  
                String t="abc";  
               }    
               catch(ArithmeticException e)  
                  {  
                   System.out.println("Arithmetic Exception occurs");  
                  }    
               catch(ArrayIndexOutOfBoundsException e)  
                  {  
                   System.out.println("ArrayIndexOutOfBounds Exception occurs");  
                  }
               catch(NullPointerException e)
               {
                   System.out.println("NullPointerException occurs");
               }
               catch(NumberFormatException e )
               {
                   System.out.println("NumberFormatException occurs");
               }
               catch(Exception e)  
                  {  
                   System.out.println("Parent Exception occurs");  
                  }             
               System.out.println("rest of the code");    
    }  
} 